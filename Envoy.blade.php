@servers(['web' => ['deployer@yiiandme.com']])

@setup
    // The timezone your servers run in
    $timezone = 'Europe/Amsterdam';

    // The base path where your deployments are sitting
    $path = "/websites/live/dynamiek-designs.com";

    // The reposititory
    $repository = "git@gitlab.com:amavis442/dynamiek-designs.git";

    // How many releases to keep
    $keep = 5;

    // Name of new release folder
    $release = (new DateTime('now', new DateTimeZone($timezone)))->format('YmdHis');

    // Folders to use
    $app_dir = $path;
    $releases_dir = $path. "/releases";
    $new_release_dir = $releases_dir ."/". $release;

    // The git branch to deploy
    $branch = isset($branch) ? $branch : "master";

@endsetup

@task('git')
    echo "Cloning repository"
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 -b {{ $branch }} "{{ $repository }}" {{ $new_release_dir }}
    echo "Done: cloning"
@endtask

@task('composer:install')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist -q -o
    echo "Done: installing composer dependencies"
@endtask

@task('artisan:publish')
    echo "Artisan publish ({{ $release }})"
    cd {{ $new_release_dir }}
    php artisan vendor:publish --all
    echo "Done: publishing assets"
@endtask

@task('artisan:clear')
    echo "Clear caches ({{ $release }})"
    cd {{ $new_release_dir }}
    php artisan config:clear
    php artisan view:clear
    php artisan route:clear
    echo "Done: clearing caches"
@endtask

@task('assets:install')
    echo "Installing assets ({{ $release }})"
    cd {{ $new_release_dir }}
    npm install
    echo "Done: installing assets"
@endtask

@task('assets:build')
    echo "Building assets ({{ $release }})"
    cd {{ $new_release_dir }}
    npm run production
    echo "Done: building assets"
@endtask

@task('symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage
    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env
@endtask

@task('live')
    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('clean')
    echo "Clean old releases"
    cd {{ $releases_dir }}
    rm -rf $(ls -t | tail -n +{{ $keep }})
    echo "Done: old releases cleaned"
@endtask

@task('php')
    echo "restart php7.2.fpm"
    systemctl restart php7.2-fpm
    echo "Restarted php"
@endtask

@story('deploy')
    git
    composer:install
    artisan:publish
    artisan:clear
    assets:install
    symlinks
    assets:build
    live
    clean
@endstory
